﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FoodInfo.aspx.cs" Inherits="VnCook.FoodInfo" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <title>VnCook - Hướng dẫn nấu ăn</title>
    <link rel="stylesheet" href="styles/Style.css">
    <link rel="stylesheet" href="styles/FoodInfo.css">
    <link rel="icon" href="images/icon.png">
    <script src="javascript/javascript.js"></script> 
    <script src="javascript/FoodInfo.js"></script>
</head>
<body>
    <div class = "narbar">
        <button id = "menu" onclick="menuClick()">MENU</button>
        <img src="images/banner.png" alt="banner" onclick="location.href = 'Index.aspx'">
        <div class="grid">
            <div class= "search" id = "idSearch">
                <input type="text" placeholder="Tìm tên món" onkeypress="return runScript(event)" id="textSearch">
                <button></button>    
            </div>     
            <div class="get-user" id = "idGetUser">
                <input class = "button" type="button" value="Đăng ký/ Đăng Nhập" id="buttonSignIn" runat="server" onclick="location.href = 'SignIn.aspx'">
                <div class="main-user" id="mainUser" runat="server">
                    <p id="userName" runat="server">Phạm Tiến Đạt</p>
                    <img id="userAvatar" runat="server" src="images/user.png">
                </div>
            </div>
        </div>
    </div>
    <hr>


    <img src="images/banner22.png" alt="banner2" class="banner">

    <div class="midd" runat="server" id="mid">
        <div class="midd-left">
            <div class="user">
                <img src="images/user.png" runat="server" id="authorAvatar">
                <div>
                    <b runat="server" id="authorName">Phạm Tiến Đạt</b>
                    <p runat="server" id="ngayDang">11/07/2020</p>
                </div>
            </div>
            <h3 runat="server" id="foodName">Bơ hấp trứng cách thủy</h3>    
            <img runat="server" id="foodImage" src="#"> 
        </div>
        
        <div class="midd-right">
            <fieldset>
                <legend>Hướng Dẫn</legend>

                <div class="thong-tin-nhanh">
                    <figure>
                        <img src="images/bua-an.png">
                        <figcaption runat="server" id="buAn"></figcaption>
                    </figure>
                    <figure>
                        <img src="images/loai.png">
                        <figcaption runat="server" id="loaiMonAn"></figcaption>
                    </figure>
                    <figure>
                        <img src="images/thoi-gian.png">
                        <figcaption runat="server" id="thoiGianNau"></figcaption>
                    </figure>
                    <figure>
                        <img src="images/view.png">
                        <figcaption runat="server" id="luotXem"></figcaption>
                    </figure>
                </div>
                <br>
                <hr>
                <div class="nguyen-lieu">
                    <h4>Nguyên Liệu</h4>
                    <br>
                    <p runat="server" id="nguyenLieuContent"></p>
                </div>
        
                <br>
                <hr>
                <div class="cach-lam">
                    <h4>Cách làm</h4>
                    <br>
                    <p runat="server" id="cachLamContent"></p>
                </div>

                <br>
                <hr>
                <form runat="server" method="post">
                    <div class="list-button" id="listButton" runat="server">
                        <input type="button" class="button button-edit" value="Sửa" runat="server" id="btnSua">
                        <button type="submit" runat="server" class="button button-delete" id="btnXoa" onserverclick="btnXoaClick" onclick="if (!btnXoaClick()) return true;">Xóa</button>
                    </div>
                </form>
            </fieldset>
            
        </div>
        
        
    </div>

    <footer>
        <p>Bản quyền của © VnCooK Inc. All Rights Reserved </p>
    </footer>
    
</body>
</html>