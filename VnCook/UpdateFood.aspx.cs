﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VnCook
{
    public partial class UpdateFood : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadComboBox();

            //Bắt đầuu chiến
            if (Request.QueryString.Get("action") != null)
            {
                //sự kiện thêm món ăn
                if (Request.QueryString.Get("action") == "1")
                {
                    if (Session["userCode"] == null)
                    {
                        fieldSet.Style.Add("border", "none");
                        fieldSet.InnerHtml = "<h3>Vui lòng đăng nhập trước khi thêm món ăn mới !<h3>";
                        return;
                    }
                    //Khởi tạo các giá trị bạn đầu
                    legend.InnerText = "Thêm món ăn mới";
                    btnSubmit.Value = "Thêm";
                }
                // sự kiện chỉnh sửa món ăn
                else if (Request.QueryString.Get("action") == "2")
                {
                    if (Session["userCode"] == null)
                    {
                        fieldSet.Style.Add("border", "none");
                        fieldSet.InnerHtml = "<h3>Vui lòng đăng nhập trước khi chỉnh sửa món ăn !<h3>";
                        return;
                    }
                    loadFood();
                    legend.InnerText = "Chỉnh sửa món ăn";
                    btnSubmit.Value = "Chỉnh sửa";
                }else{
                    fieldSet.Style.Add("border", "none");
                    fieldSet.InnerHtml = "<h3>Không tìm thấy link !<h3>";
                }
            }
            else
            {
                fieldSet.Style.Add("border", "none");
                fieldSet.InnerHtml = "<h3>Không tìm thấy link !<h3>";
            }

            //Xử lý narbar
            xuLyNarBar();


        }

        //Thêm món ăn mới
        protected void addFood()
        {
            String monAnCode = "MA_" + (DataUtils.getSumRowFromTable("tbl_mon_an") + 1);
            String userCode = Session["userCode"].ToString();


            StringBuilder sql = new StringBuilder("INSERT INTO tbl_mon_an VALUES(");
            sql.Append(DataUtils.getStringSql(monAnCode) + ",");
            sql.Append(DataUtils.getStringSqlVi(Request.Form.Get("tenMonAn")) + ",");
            sql.Append(DataUtils.getStringSql(Request.Form.Get("loaiMonAn")) + ",");
            sql.Append(DataUtils.getStringSql(Request.Form.Get("thoiGianNau")) + ",");
            sql.Append(DataUtils.getStringSql(Request.Form.Get("buaAn")) + ",");


            //Xử lý thêm ảnh !
            if ((img.PostedFile != null) && (img.PostedFile.ContentLength > 0))
            {
                //Get file name
                //string fn = System.IO.Path.GetFileName(image.PostedFile.FileName);

                //Get định dạng của file, .jpg, .png
                String fn = System.IO.Path.GetExtension(img.PostedFile.FileName);
                String nameImage = DateTime.Now.ToFileTime() + "_" + userCode + fn;

                String SaveLocation = Server.MapPath(@"images\foods\") + nameImage;

                sql.Append(DataUtils.getStringSql(@"images/foods/" + nameImage) + ",");

                try
                {
                    img.PostedFile.SaveAs(SaveLocation);
                }
                catch (Exception ex)
                {
                    Response.Write(SaveLocation);
                    Response.Write("Error: " + ex.Message);
                }
            }
            else
            {
                loiAnh.InnerHtml = "<br>Vui lòng thêm ảnh món ăn !";
                loiAnh.Style.Add("color", "red");
                return;
            }

            //Continues
            sql.Append(DataUtils.getStringSql(Constants.STATUS_ACTIVE) + ",");
            sql.Append(DataUtils.getStringSql(userCode) + ",");
            sql.Append(DataUtils.getStringSql(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + ",");
            sql.Append(DataUtils.getStringSql(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + ",");
            sql.Append(DataUtils.getStringSqlVi(Request.Form.Get("txtNguyenLieu")) + ",");
            sql.Append(DataUtils.getStringSqlVi(Request.Form.Get("txtCachLam")) + ",0)");

            //Thực hiện thêm
            int res = DataProvider.execNonQuery(sql.ToString());

            if (res > 0)
            {
                legend.InnerText = "Thêm món ăn mới ( Đã thêm thành công !)";
            }
            
        }

        //Chỉnh sửa món ăn
        protected void editFood()
        {
            StringBuilder sql = new StringBuilder("UPDATE tbl_mon_an SET ");
            sql.Append("ten_mon_an = " + DataUtils.getStringSqlVi(Request.Form.Get("tenMonAn")) + ",");
            sql.Append("loai_mon_an_code = " + DataUtils.getStringSqlVi(Request.Form.Get("loaiMonAn")) + ",");
            sql.Append("thoi_gian_nau_code = " + DataUtils.getStringSqlVi(Request.Form.Get("thoiGianNau")) + ",");
            sql.Append("bua_an_code = " + DataUtils.getStringSqlVi(Request.Form.Get("buaAn")) + ",");

            //Xử lý thêm ảnh !
            if ((img.PostedFile != null) && (img.PostedFile.ContentLength > 0))
            {
                //Get file name
                //string fn = System.IO.Path.GetFileName(image.PostedFile.FileName);

                //Get định dạng của file, .jpg, .png
                String fn = System.IO.Path.GetExtension(img.PostedFile.FileName);
                String nameImage = DateTime.Now.ToFileTime() + "_" + Session["userCode"] + fn;

                String SaveLocation = Server.MapPath(@"images\foods\") + nameImage;

                sql.Append("image = " + DataUtils.getStringSql(@"images/foods/" + nameImage) + ",");

                try
                {
                    img.PostedFile.SaveAs(SaveLocation);
                }
                catch (Exception ex)
                {
                    Response.Write(SaveLocation);
                    Response.Write("Error: " + ex.Message);
                }
            }

            //Continues
            sql.Append("approved_date = " + DataUtils.getStringSql(DateTime.Now.ToString()) + ",");
            sql.Append("nguyen_lieu = " + DataUtils.getStringSqlVi(Request.Form.Get("txtNguyenLieu")) + ",");
            sql.Append("cach_lam = " + DataUtils.getStringSqlVi(Request.Form.Get("txtCachLam")));
            sql.Append(" WHERE mon_an_code = " + DataUtils.getStringSql(Request.QueryString.Get("foodCode").ToString()));

            //Thực hiện thêm
            int res = DataProvider.execNonQuery(sql.ToString());

            if (res > 0)
            {
                legend.InnerText = "Chỉnh sửa món ăn( Đã chỉnh sửa thành công !)";
            }
        }

        //load danh sách dữ liệu trong commbox
        protected void loadComboBox()
        {
            DataTable dt = new DataTable();
            //Lấy danh sách các bữa ăn
            dt = DataUtils.getAllDataFromTable("tbl_bua_an");
            foreach (DataRow row in dt.Rows)
            {
                buaAn.Items.Add(new ListItem(row["label_bua_an"].ToString(), row["bua_an_code"].ToString()));
            }

            //Lấy danh sách loại món ăn
            dt = DataUtils.getAllDataFromTable("tbl_loai_mon_an");
            foreach (DataRow row in dt.Rows)
            {
                loaiMonAn.Items.Add(new ListItem(row["label_loai_mon_an"].ToString(), row["loai_mon_an_code"].ToString()));
            }

            //Lấy danh sách các bữa ăn
            dt = DataUtils.getAllDataFromTable("tbl_thoi_gian_nau");
            foreach (DataRow row in dt.Rows)
            {
                thoiGianNau.Items.Add(new ListItem(row["label_thoi_gian_nau"].ToString(), row["thoi_gian_nau_code"].ToString()));
            }
        }

        //loadFood()
        protected void loadFood()
        {
            if (Request.QueryString.Get("foodCode") == null)
            {
                fieldSet.Style.Add("border", "none");
                fieldSet.InnerHtml = "<h3>Không tìm thấy link !<h3>";
                return;
            }

            DataTable food;
            String foodCode = Request.QueryString.Get("foodCode").ToString();
            StringBuilder sql = new StringBuilder("SELECT * FROM tbl_mon_an");
            sql.Append(" WHERE mon_an_code = " + DataUtils.getStringSql(foodCode));
            sql.Append(" AND status = " + DataUtils.getStringSql(Constants.STATUS_ACTIVE));

            food = DataProvider.execQuery(sql.ToString());

            if (food.Rows.Count == 0)
            {
                fieldSet.Style.Add("border", "none");
                fieldSet.InnerHtml = "<h3>Không tìm thấy link !<h3>";
                return;
            }

            if (Session["userCode"].ToString() != food.Rows[0]["author"].ToString())
            {
                fieldSet.Style.Add("border", "none");
                fieldSet.InnerHtml = "<h3>Bạn không có quyền sửa món ăn này !<h3>";
                return;
            }

            //Nếu có quyền các thứ các thứ
            tenMonAn.Value = food.Rows[0]["ten_mon_an"].ToString();
            
            buaAn.DataBind();
            buaAn.Items.FindByValue(food.Rows[0]["bua_an_code"].ToString()).Selected = true;

            loaiMonAn.DataBind();
            loaiMonAn.Items.FindByValue(food.Rows[0]["loai_mon_an_code"].ToString()).Selected = true;
            
            thoiGianNau.DataBind();
            thoiGianNau.Items.FindByValue(food.Rows[0]["thoi_gian_nau_code"].ToString()).Selected = true;
            

            imgPreview.Src = food.Rows[0]["image"].ToString();

            txtNguyenLieu.Value = food.Rows[0]["nguyen_lieu"].ToString();
            txtCachLam.Value = food.Rows[0]["cach_lam"].ToString();

        }

        //Bắt sự kiện click nút
        protected void btnSubmitClick(object sender, EventArgs e)
        {
            if (Request.QueryString.Get("action") == "1")
            {
                addFood();
            }
            else
            {
                editFood();
                loadFood();
            }
        }

        protected void xuLyNarBar()
        {
            if (Session["userCode"] != null)
            {
                userName.InnerHtml = "<a href = 'Profile.aspx?userCode=" + Session["userCode"].ToString() + "'>" + Session["userName"].ToString() + "</a>";
                userAvatar.Src = Session["userAvatar"].ToString();
                mainUser.Style.Add("display", "flex");
                buttonSignIn.Style.Add("visibility", "hidden");
            }
        }
    }
}