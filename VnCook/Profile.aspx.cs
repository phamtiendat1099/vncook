﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VnCook
{
    public partial class Profile : System.Web.UI.Page
    {
        String userCode = "";
        Dictionary<string, string> mapComboBox = new Dictionary<string, string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            userCode = Request.QueryString.Get("userCode");

            if (userCode != null)
            {
                StringBuilder sql = new StringBuilder();

                sql.Append(" SELECT * FROM tbl_user");
                sql.Append(" WHERE USER_CODE = " + DataUtils.getStringSql(userCode));

                DataTable dt = DataProvider.execQuery(sql.ToString());

                if (dt.Rows.Count > 0)
                {
                    txtemail.Value = dt.Rows[0]["email"].ToString();
                    txtname.Value = dt.Rows[0]["ho_ten"].ToString();
                    txtsdt.Value = dt.Rows[0]["so_dien_thoai"].ToString();

                    if (dt.Rows[0]["gioi_tinh"] != null)
                    {
                        if (dt.Rows[0]["gioi_tinh"].ToString() == Constants.GIOITINH_NAM)
                        {
                            lbGioiTinh.InnerHtml = "Giới Tính: <b>Nam</b>";
                        }
                        else if (dt.Rows[0]["gioi_tinh"].ToString() == Constants.GIOITINH_NU)
                        {
                            lbGioiTinh.InnerHtml = "Giới Tính: <b>Nữ</b>";
                        }
                    }

                    avatar.Src = dt.Rows[0]["avatar"].ToString();

                    if (Session["userCode"]!=null)
                    {
                        if (Session["userCode"].ToString() == userCode)
                        {
                            btnSua.Style.Add("display", "inline-block");
                            btnLogout.Style.Add("display", "inline-block");
                            btnCreate.Style.Add("display", "inline-block");
                        }
                        else
                        {
                            tongSoMonAn.Style.Add("margin-left", "25px");
                        }
                    }
                    else
                    {
                        tongSoMonAn.Style.Add("margin-left", "25px");
                    }

                    //đếm số món ăn đã nấu
                    sql = new StringBuilder("SELECT COUNT(1) FROM tbl_mon_an ");
                    sql.Append(" WHERE author = " + DataUtils.getStringSql(userCode));
                    sql.Append(" AND status = " + DataUtils.getStringSql(Constants.STATUS_ACTIVE));

                    tongSoMonAn.InnerText = "Tổng số món đã đăng: "+(int)DataProvider.execScaler(sql.ToString());
                    
                    //load map
                    loadMapComboBox();
                    //load danh sách món ăn
                    loadDsMonAn();

                }
                else
                {
                    mid.InnerHtml = "<h3>Không tìm thấy link !</h3>";
                }
            }
            else
            {
                mid.InnerHtml = "<h3>Không tìm thấy link !</h3>";
            }

            //Xử lý narbar
            xuLyNarBar();

        }

        protected void btnOKClick(object sender, EventArgs e)
        {
            StringBuilder sql = new StringBuilder("UPDATE tbl_user ");
            sql.Append("SET ho_ten = N" + DataUtils.getStringSql(Request.Form.Get("txtname")) + ",");
            sql.Append("so_dien_thoai = " + DataUtils.getStringSql(Request.Form.Get("txtsdt")) + ",");

            Session["userName"] = Request.Form.Get("txtname");

            sql.Append("gioi_tinh = " + DataUtils.getStringSql(Request.Form.Get("gioiTinh")));


            if ((image.PostedFile != null) && (image.PostedFile.ContentLength > 0))
            {
                //Get file name
                //string fn = System.IO.Path.GetFileName(image.PostedFile.FileName);

                //Get định dạng của file, .jpg, .png
                String fn = System.IO.Path.GetExtension(image.PostedFile.FileName);
                String nameImage =  DateTime.Now.ToFileTime() + "_" + Request.QueryString.Get("userCode") + fn;

                String SaveLocation = Server.MapPath(@"images\avatar\") + nameImage;

                sql.Append(", avatar = " + DataUtils.getStringSql(@"images/avatar/" + nameImage));
                Session["userAvatar"] = "images/avatar/" + nameImage;

                try
                {
                    image.PostedFile.SaveAs(SaveLocation);
                }
                catch (Exception ex)
                {
                    Response.Write(SaveLocation);
                    Response.Write("Error: " + ex.Message);
                }
            }

            sql.Append(" WHERE USER_CODE = " + DataUtils.getStringSql(userCode));

            //Thực hiện thêm vào CSDL
            DataProvider.execNonQuery(sql.ToString());
            Response.Redirect(Request.RawUrl);
        }

        protected void logOutClick(object sender, EventArgs e)
        {
            Session.Remove("email");
            Session.Remove("userCode");
            Session.Remove("userName");
            Session.Remove("userAvatar");
            Response.Redirect("Index.aspx");
        }

        protected void loadDsMonAn()
        {
            DataTable dt = DataUtils.getAllDataFromTable("tbl_mon_an", userCode);
            StringBuilder html = new StringBuilder();
            foreach (DataRow row in dt.Rows)
            {
                html.Append("<div class = 'mon-an'>");
                html.Append("<h4><a href='FoodInfo.aspx?foodCode="+row["mon_an_code"]+"'>" + row["ten_mon_an"] + "</a></h4>");
                html.Append("<p>Ngày đăng:" +DataUtils.formatDate(row["approved_date"].ToString()) + "</p>");
                html.Append("<img class='anh-bia' src='" + row["image"] + "'>");
                html.Append("<div class='status'>");
                html.Append("<figure>");
                html.Append("<img src='images/bua-an.png'>");
                html.Append("<figcaption>" + mapComboBox[row["bua_an_code"].ToString()] + "</figcaption>");
                html.Append("</figure>");

                html.Append("<figure>");
                html.Append("<img src='images/loai.png'>");
                html.Append("<figcaption>" + mapComboBox[row["loai_mon_an_code"].ToString()] + "</figcaption>");
                html.Append("</figure>");

                html.Append("<figure>");
                html.Append("<img src='images/thoi-gian.png'>");
                html.Append("<figcaption>" + mapComboBox[row["thoi_gian_nau_code"].ToString()] + "</figcaption>");
                html.Append("</figure>");

                html.Append("<figure>");
                html.Append("<img src='images/view.png'>");
                html.Append("<figcaption>" + row["luot_xem"].ToString() + " lượt xem"  + "</figcaption>");
                html.Append("</figure>");

                html.Append("</div></div>");
            }
            lissMonAn.InnerHtml = html.ToString();
        }

        protected void loadMapComboBox()
        {
            DataTable dt = new DataTable();
            
            //Lấy danh sách các bữa ăn
            dt = DataUtils.getAllDataFromTable("tbl_bua_an");
            foreach (DataRow row in dt.Rows)
            {
                mapComboBox.Add(row["bua_an_code"].ToString(), row["label_bua_an"].ToString());
            }

            //Lấy danh sách loại món ăn
            dt = DataUtils.getAllDataFromTable("tbl_loai_mon_an");
            foreach (DataRow row in dt.Rows)
            {
                mapComboBox.Add(row["loai_mon_an_code"].ToString(), row["label_loai_mon_an"].ToString());
            }

            //Lấy danh sách các bữa ăn
            dt = DataUtils.getAllDataFromTable("tbl_thoi_gian_nau");
            foreach (DataRow row in dt.Rows)
            {
                mapComboBox.Add(row["thoi_gian_nau_code"].ToString(), row["label_thoi_gian_nau"].ToString());
            }
            
        }

        protected void xuLyNarBar()
        {
            if (Session["userCode"] != null)
            {
                userName.InnerHtml = "<a href = 'Profile.aspx?userCode=" + Session["userCode"].ToString() + "'>" + Session["userName"].ToString() + "</a>";
                userAvatar.Src = Session["userAvatar"].ToString();
                mainUser.Style.Add("display", "flex");
                buttonSignIn.Style.Add("visibility", "hidden");
            }
        }
    }
}