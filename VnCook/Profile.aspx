﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="VnCook.Profile" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <title>VnCook - Hướng dẫn nấu ăn</title>
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/profile.css">
    <link rel="icon" href="images/icon.png"> 
    <script src="javascript/profile.js"></script>
    <script src="javascript/javascript.js"></script>
</head>
<body>
    <div class = "narbar">
        <button id = "menu" onclick="menuClick()">MENU</button>
        <img src="images/banner.png" alt="banner" onclick="location.href = 'Index.aspx'">
        <div class="grid">
            <div class= "search" id = "idSearch">
                <input type="text" placeholder="Tìm tên món" onkeypress="return runScript(event)" id="textSearch">
                <button></button>    
            </div>     
            <div class="get-user" id = "idGetUser">
                <input class = "button" type="button" value="Đăng ký/ Đăng Nhập" id="buttonSignIn" runat="server" onclick="location.href = 'SignIn.aspx'">
                <div class="main-user" id="mainUser" runat="server">
                    <p id="userName" runat="server">Phạm Tiến Đạt</p>
                    <img id="userAvatar" runat="server" src="images/user.png">
                </div>
            </div>
        </div>
    </div>
    <hr>

    <!-- <img src="images/banner22.png" alt="banner2" class="banner"> -->
    
    <div class="mid" id="mid" runat="server">
        <form method="POST" runat="server">        
            <div class="profile" runat="server">    
                <img id = "avatar" src="images/dat.jpg" runat="server">
                <input id = "image" name="nameimage" type="file" accept="image/*" onchange="changeAvatar()" runat="server">
                <div class="info" runat="server">
                    <label for="email">Email:</label>
                    <input name="email" type="text" value="phamtiendat1099@gmail.com" class="txt" id = "txtemail" runat="server" readonly>
                    <br>
                    <label for="name">Họ Tên:</label>
                    <input name="name" type="text" value="Phạm Tiến Đạt" class="txt" id = "txtname" runat="server" readonly>
                    <br>
                    <label for="sdt">Số Điện Thoại:</label>
                    <input name="sdt" type="text" value="0352263257" class="txt" id = "txtsdt" runat="server" readonly>
                    <br>
                    <label for="gioiTinh" id="lbGioiTinh" runat="server">Giới Tính:</label>
                    <div id ="listRadio" runat="server">
                        <input name="gioiTinh" type="radio" value="1" checked> nam
                        <input name="gioiTinh" type="radio" value="0"> nữ
                    </div>
                    <br>
                    <input type="button" class="button button-edit" id = "btnSua" value="Sửa" onclick="editProfile()" runat="server">
                    <input type="submit" class="button button-edit" id = "btnOk" value="OK" onclick="okProfile()" runat="server" onserverclick="btnOKClick">
                    <input type="button" class="button button-edit" id = "btnLogout" value="Đăng xuất" runat="server" onserverclick="logOutClick" >
                </div>  
            </div>
        </form>

        <div class="data">
            <fieldset>
                <legend>Danh sách món</legend>
                <input type="button" id="btnCreate" value="Thêm món mới" class="button button-edit" runat="server" onclick="addNewFood()">
                <b id="tongSoMonAn" runat="server">Tổng số: 200 món ăn</b>
                <br>
                <hr>
                <div class="list-mon-an" id="lissMonAn" runat="server">
                    <div class = "mon-an">
                        <h4><a href="#">Bơ hấp trứng cách thủy</a></h4>
                        <p>Ngày đăng: 14/07/2020</p>
                        <img class="anh-bia" src="https://img-global.cpcdn.com/recipes/404d11c5f17d120d/640x640sq70/photo.jpg">
                        <div class="status">
                            <figure>
                                <img src="images/bua-an.png">
                                <figcaption>Bữa sáng</figcaption>
                            </figure>
                            <figure>
                                <img src="images/loai.png">
                                <figcaption>Món nấu</figcaption>
                            </figure>
                            <figure>
                                <img src="images/thoi-gian.png">
                                <figcaption>15 phút</figcaption>
                            </figure>
                        </div>
                    </div>  
                    
                    <div class = "mon-an">
                        <h4><a href="#">Bơ hấp trứng cách thủy</a></h4>
                        <p>Ngày đăng: 14/07/2020</p>
                        <img class="anh-bia" src="https://img-global.cpcdn.com/recipes/404d11c5f17d120d/640x640sq70/photo.jpg">
                        <div class="status">
                            <figure>
                                <img src="images/bua-an.png">
                                <figcaption>Bữa sáng</figcaption>
                            </figure>
                            <figure>
                                <img src="images/loai.png">
                                <figcaption>Món nấu</figcaption>
                            </figure>
                            <figure>
                                <img src="images/thoi-gian.png">
                                <figcaption>15 phút</figcaption>
                            </figure>
                        </div>
                    </div>    

                    <div class = "mon-an">
                        <h4><a href="#">Bơ hấp trứng cách thủy</a></h4>
                        <p>Ngày đăng: 14/07/2020</p>
                        <img class="anh-bia" src="https://img-global.cpcdn.com/recipes/404d11c5f17d120d/640x640sq70/photo.jpg">
                        <div class="status">
                            <figure>
                                <img src="images/bua-an.png">
                                <figcaption>Bữa sáng</figcaption>
                            </figure>
                            <figure>
                                <img src="images/loai.png">
                                <figcaption>Món nấu</figcaption>
                            </figure>
                            <figure>
                                <img src="images/thoi-gian.png">
                                <figcaption>15 phút</figcaption>
                            </figure>
                        </div>
                    </div>    

                    <div class = "mon-an">
                        <h4><a href="#">Bơ hấp trứng cách thủy</a></h4>
                        <p>Ngày đăng: 14/07/2020</p>
                        <img class="anh-bia" src="https://img-global.cpcdn.com/recipes/404d11c5f17d120d/640x640sq70/photo.jpg">
                        <div class="status">
                            <figure>
                                <img src="images/bua-an.png">
                                <figcaption>Bữa sáng</figcaption>
                            </figure>
                            <figure>
                                <img src="images/loai.png">
                                <figcaption>Món nấu</figcaption>
                            </figure>
                            <figure>
                                <img src="images/thoi-gian.png">
                                <figcaption>15 phút</figcaption>
                            </figure>
                        </div>
                    </div>    

                    <div class = "mon-an">
                        <h4><a href="#">Bơ hấp trứng cách thủy</a></h4>
                        <p>Ngày đăng: 14/07/2020</p>
                        <img class="anh-bia" src="https://img-global.cpcdn.com/recipes/404d11c5f17d120d/640x640sq70/photo.jpg">
                        <div class="status">
                            <figure>
                                <img src="images/bua-an.png">
                                <figcaption>Bữa sáng</figcaption>
                            </figure>
                            <figure>
                                <img src="images/loai.png">
                                <figcaption>Món nấu</figcaption>
                            </figure>
                            <figure>
                                <img src="images/thoi-gian.png">
                                <figcaption>15 phút</figcaption>
                            </figure>
                        </div>
                    </div>    

                    <div class = "mon-an">
                        <h4><a href="#">Bơ hấp trứng cách thủy</a></h4>
                        <p>Ngày đăng: 14/07/2020</p>
                        <img class="anh-bia" src="https://img-global.cpcdn.com/recipes/404d11c5f17d120d/640x640sq70/photo.jpg">
                        <div class="status">
                            <figure>
                                <img src="images/bua-an.png">
                                <figcaption>Bữa sáng</figcaption>
                            </figure>
                            <figure>
                                <img src="images/loai.png">
                                <figcaption>Món nấu</figcaption>
                            </figure>
                            <figure>
                                <img src="images/thoi-gian.png">
                                <figcaption>15 phút</figcaption>
                            </figure>
                        </div>
                    </div>    
                </div>
            </fieldset>
        </div>

    </div>
    

    <footer>
        <p>Bản quyền của © VnCooK Inc. All Rights Reserved </p>
    </footer>
    
</body>
</html>
