﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace VnCook
{
    public static class Constants
    {
        public static String STATUS_ACTIVE = "1";
        public static String STATUS_INACTIVE = "0";
        public static String ROLE_AMDIN = "1";
        public static String ROLE_USER = "2";
        public static String GIOITINH_NAM = "1";
        public static String GIOITINH_NU = "0";
    }
}
