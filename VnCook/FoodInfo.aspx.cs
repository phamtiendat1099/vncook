﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VnCook
{
    public partial class FoodInfo : System.Web.UI.Page
    {
        DataTable dt;
        String foodCode;
        Dictionary<string, string> mapComboBox = new Dictionary<string, string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Get("foodCode")==null)
            {
                mid.InnerHtml = "<h3>Không tìm thấy link !</h3><br>";
                return;
            }

            foodCode = Request.QueryString.Get("foodCode").ToString();
            StringBuilder sql = new StringBuilder("SELECT * FROM tbl_mon_an");
            sql.Append(" WHERE mon_an_code = " + DataUtils.getStringSql(foodCode));
            sql.Append(" AND status = " + DataUtils.getStringSql(Constants.STATUS_ACTIVE));

            dt = DataProvider.execQuery(sql.ToString());

            if (dt.Rows.Count == 0) {
                mid.InnerHtml = "<h3>Không tìm thấy link !</h3><br>";
                return;
            }

            //Xử lý narbar
            xuLyNarBar();

            //load dữ liệu map
            loadMapComboBox();

            //Cộng lượt xem bài viết
            congLuotXem();

            //đổ dữ liệu hiển thị
            DataTable dtt = DataUtils.getUserInforFromUserCode(dt.Rows[0]["author"].ToString());
            authorAvatar.Src = dtt.Rows[0]["avatar"].ToString();
            authorName.InnerHtml = "<a href = 'Profile.aspx?userCode="+dt.Rows[0]["author"].ToString()+"'>"+dtt.Rows[0]["ho_ten"].ToString()+"</a>";

            ngayDang.InnerText = DataUtils.formatDate(dt.Rows[0]["approved_date"].ToString());
            luotXem.InnerText = dt.Rows[0]["luot_xem"].ToString() + " lượt xem";
            foodName.InnerText = dt.Rows[0]["ten_mon_an"].ToString();
            foodImage.Src = dt.Rows[0]["image"].ToString();

            buAn.InnerText = mapComboBox[dt.Rows[0]["bua_an_code"].ToString()];
            thoiGianNau.InnerText = mapComboBox[dt.Rows[0]["thoi_gian_nau_code"].ToString()];
            loaiMonAn.InnerText = mapComboBox[dt.Rows[0]["loai_mon_an_code"].ToString()];

            nguyenLieuContent.InnerHtml = dt.Rows[0]["nguyen_lieu"].ToString().Replace("\r\n", "<br>"); ;
            cachLamContent.InnerHtml = dt.Rows[0]["cach_lam"].ToString().Replace("\r\n", "<br>");

            if (Session["userCode"] !=null)
            {
                if (Session["userCode"].ToString() == dt.Rows[0]["author"].ToString())
                    listButton.Style.Add("display", "inline-block");

                btnSua.Attributes.Add("onclick", "location.href = 'UpdateFood.aspx?action=2&foodCode=" + dt.Rows[0]["mon_an_code"].ToString()+"'");
            }
        }

        public void btnXoaClick(object sender, EventArgs e)
        {
            StringBuilder sql = new StringBuilder("UPDATE tbl_mon_an");
            sql.Append(" SET status = " + DataUtils.getStringSql(Constants.STATUS_INACTIVE));
            sql.Append(" WHERE mon_an_code = " + DataUtils.getStringSql(foodCode));

            DataProvider.execNonQuery(sql.ToString());

            Response.Redirect("Index.aspx");
        }

        protected void loadMapComboBox()
        {
            DataTable dt = new DataTable();
            //Lấy danh sách các bữa ăn
            dt = DataUtils.getAllDataFromTable("tbl_bua_an");
            foreach (DataRow row in dt.Rows)
            {
                mapComboBox.Add(row["bua_an_code"].ToString(), row["label_bua_an"].ToString());
            }

            //Lấy danh sách loại món ăn
            dt = DataUtils.getAllDataFromTable("tbl_loai_mon_an");
            foreach (DataRow row in dt.Rows)
            {
                mapComboBox.Add(row["loai_mon_an_code"].ToString(), row["label_loai_mon_an"].ToString());
            }

            //Lấy danh sách các bữa ăn
            dt = DataUtils.getAllDataFromTable("tbl_thoi_gian_nau");
            foreach (DataRow row in dt.Rows)
            {
                mapComboBox.Add(row["thoi_gian_nau_code"].ToString(), row["label_thoi_gian_nau"].ToString());
            }
        }

        protected void xuLyNarBar()
        {
            if (Session["userCode"] != null)
            {
                userName.InnerHtml = "<a href = 'Profile.aspx?userCode=" + Session["userCode"].ToString() + "'>" + Session["userName"].ToString() + "</a>";
                userAvatar.Src = Session["userAvatar"].ToString();
                mainUser.Style.Add("display", "flex");
                buttonSignIn.Style.Add("visibility", "hidden");
            }
        }

        protected void congLuotXem()
        {
            StringBuilder sql = new StringBuilder("UPDATE tbl_mon_an ");
            sql.Append(" SET luot_xem = luot_xem + 1");
            sql.Append(" WHERE mon_an_code = " + DataUtils.getStringSql(foodCode));

            DataProvider.execNonQuery(sql.ToString());
        }

    }
}