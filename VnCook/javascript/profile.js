function editProfile() {
    document.getElementById("txtname").removeAttribute("readonly");
    document.getElementById("txtsdt").removeAttribute("readonly");
    document.getElementById("listRadio").style.visibility = "visible";

    document.getElementById("txtname").style.border = "1px solid #b3b3b3";
    document.getElementById("txtsdt").style.border = "1px solid #b3b3b3";

    document.getElementById("btnOk").style.display = "inline-block";

    document.getElementById("avatar").src = "https://cdn1.iconfinder.com/data/icons/internet-and-security-outline-1/64/internet-and-security-outline-1-04-512.png";

    document.getElementById("avatar").addEventListener('click', function(){
        previewImage = document.getElementById("image").click();
    });
}


function changeAvatar(){
    var reader = new FileReader();
    reader.onload = function()
    {
        var output = document.getElementById('avatar');
        output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
}

function okProfile(){
    document.getElementById("txtname").readOnly = true;
    document.getElementById("txtsdt").readOnly = true;
    document.getElementById("listRadio").style.visibility = "hidden";

    document.getElementById("txtname").style.border = "none";
    document.getElementById("sdt").style.border = "none";

    document.getElementById("btnOk").style.display = "none";

    //xóa cái event bắt sự kiện bắt
    var old_element = document.getElementById("avatar");
    var new_element = old_element.cloneNode(true);
    old_element.parentNode.replaceChild(new_element, old_element);
}

function addNewFood(){
    window.location.href = "UpdateFood.aspx?action=1";
}
