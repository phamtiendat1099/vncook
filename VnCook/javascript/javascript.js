function menuClick(){
    if(document.getElementById("idSearch").style.display == "flex"){
        document.getElementById("idSearch").style.display = "none"
        document.getElementById("idGetUser").style.display = "none";
        document.getElementById("menu").innerHTML = "MENU";
        document.getElementById("menu").style.color = "#bc5358";
        document.getElementById("menu").style.border = "2px solid #bc5358";
        document.getElementById("menu").style.backgroundColor = "white";
    }
    else{
        document.getElementById("idSearch").style.display = "flex";
        document.getElementById("idGetUser").style.display = "flex";
        document.getElementById("menu").innerHTML = "CLOSE";
        document.getElementById("menu").style.color = "white";
        document.getElementById("menu").style.border = "#2px solid bc5358";
        document.getElementById("menu").style.backgroundColor = "#bc5358"
    }
    
}

function runScript(e) {
    //See notes about 'which' and 'key'
    if (e.keyCode == 13) {
        location.href = "Search.aspx?tenMonAn=" + document.getElementById("textSearch").value;
        return false;
    }
}