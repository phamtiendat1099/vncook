﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace VnCook
{
    public static class DataUtils
    {
        public static bool getLogin(String email, String pass)
        {

            StringBuilder sql = new StringBuilder();
            sql.Append(" SELECT COUNT(*) FROM tbl_user");
            sql.Append(" WHERE email = '" + email + "'");
            sql.Append(" AND password = '" + MaHoa.GetMD5(pass) + "'");

            int res = (int) DataProvider.execScaler(sql.ToString());

            if (res == 1) return true;
            else return false;
        }

        public static String getStringSql(String st)
        {
            return "'" + st + "'";
        }

        public static String getStringSqlVi(String st)
        {
            return "N'" + st + "'";
        }

        public static DataTable getAllDataFromTable(String tableName)
        {
            StringBuilder sql = new StringBuilder("SELECT * FROM " + tableName + " ");
            sql.Append("WHERE status = " + getStringSql(Constants.STATUS_ACTIVE));

            return DataProvider.execQuery(sql.ToString());
        }

        public static DataTable getAllDataFromTable(String tableName, String author)
        {
            StringBuilder sql = new StringBuilder("SELECT * FROM " + tableName);
            sql.Append(" WHERE status = " + getStringSql(Constants.STATUS_ACTIVE));
            sql.Append(" AND author = " + getStringSql(author));

            return DataProvider.execQuery(sql.ToString());
        }


        public static DataTable getUserInforFromEmail(String email)
        {
            StringBuilder sql = new StringBuilder("SELECT * FROM tbl_user");
            sql.Append(" WHERE email = " + getStringSql(email));
            sql.Append(" AND status = " + getStringSql(Constants.STATUS_ACTIVE));

            return DataProvider.execQuery(sql.ToString());
        }

        public static DataTable getUserInforFromUserCode(String userCode)
        {
            StringBuilder sql = new StringBuilder("SELECT * FROM tbl_user");
            sql.Append(" WHERE user_code = " + getStringSql(userCode));
            sql.Append(" AND status = " + getStringSql(Constants.STATUS_ACTIVE));

            return DataProvider.execQuery(sql.ToString());
        }

        public static int getSumRowFromTableActive(String tableName)
        {
            StringBuilder sql = new StringBuilder("SELECT COUNT(1) FROM " + tableName + " ");
            sql.Append("WHERE status = " + getStringSql(Constants.STATUS_ACTIVE));

            return (int)DataProvider.execScaler(sql.ToString());
        }

        public static int getSumRowFromTable(String tableName)
        {
            StringBuilder sql = new StringBuilder("SELECT COUNT(1) FROM " + tableName + " ")
            return (int) DataProvider.execScaler(sql.ToString());
        }

        public static String formatDate(String date)
        {
            return Convert.ToDateTime(date).ToString("dd/MM/yyyy HH:mm:ss");
        }
    }
}
