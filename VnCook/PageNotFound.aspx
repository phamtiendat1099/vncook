﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PageNotFound.aspx.cs" Inherits="VnCook.WebError" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <title>VnCook - Page Error</title>
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/PageNotFound.css">
    <link rel="icon" href="images/icon.png"> 
</head>
<body>
    <img src="https://freefrontend.com/assets/img/html-css-404-page-templates/HTML-404-Page-Animated.png" />
    </body>
</html>
