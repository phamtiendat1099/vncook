﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VnCook
{
    public partial class SignIn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Xử lý narbar
            xuLyNarBar();
        }

        protected void signInClick(object sender, EventArgs e)
        {
            String email = Request.Form.Get("dang-nhap-email");
            String pass = Request.Form.Get("dang-nhap-password");

            bool res = DataUtils.getLogin(email, pass);

            if (res==true)
            {
                Session["email"] = email;

                DataTable dt = DataUtils.getUserInforFromEmail(email);
                Session["userCode"] = dt.Rows[0]["user_code"];
                Session["userName"] = dt.Rows[0]["ho_ten"];
                Session["userAvatar"] = dt.Rows[0]["avatar"];

                loiDangNhap.InnerHtml = "";

                Response.Redirect("Index.aspx");
            }
            else
            {
                loiDangNhap.InnerHtml = "Sai tài khoản hoặc mật khẩu !";
                loiDangNhap.Style.Add("color", "#e60000");
            }

        }

        protected void signUpClick(object sender, EventArgs e)
        {
            String email = Request.Form.Get("dang-ky-email");
            String pass = Request.Form.Get("dang-ky-password");

            int res = 0;
            //Kiểm tra email đã trùng chưa
            res = (int) DataProvider.execScaler("SELECT COUNT(1) FROM tbl_user WHERE email = " + DataUtils.getStringSql(email));

            if (res > 0)
            {
                loiDangKy.InnerText = "Email đã tồn tại, vui lòng kiểm tra lại !";
                loiDangKy.Style.Add("color", "#e60000");
            }
            else
            {
                //Thực hiện gen user_code
                res = (int) DataProvider.execScaler("SELECT COUNT(1) FROM tbl_user");
                String userCode = "USER_" + (res+1);

                //Thực hiện thêm mới tài khoản
                StringBuilder sql = new StringBuilder("INSERT INTO tbl_user(user_code, email, password, status, role, ho_ten) VALUES(");
                sql.Append(DataUtils.getStringSql(userCode) + ",");
                sql.Append(DataUtils.getStringSql(email) + ",");
                sql.Append(DataUtils.getStringSql(MaHoa.GetMD5(pass)) + ",");
                sql.Append(DataUtils.getStringSql(Constants.STATUS_ACTIVE) + ",");
                sql.Append(DataUtils.getStringSql(Constants.ROLE_USER) + ",'No Name')");

                res = DataProvider.execNonQuery(sql.ToString());

                if (res > 0)
                {
                    loiDangKy.InnerHtml = "Đăng ký thành công, ngay bây giờ bạn có thể đăng nhập !";
                    loiDangKy.Style.Add("color", "green");
                }
            }
            
        }

        protected void xuLyNarBar()
        {
            if (Session["userCode"] != null)
            {
                userName.InnerHtml = "<a href = 'Profile.aspx?userCode=" + Session["userCode"].ToString() + "'>" + Session["userName"].ToString() + "</a>";
                userAvatar.Src = Session["userAvatar"].ToString();
                mainUser.Style.Add("display", "flex");
                buttonSignIn.Style.Add("visibility", "hidden");
            }
        }

    }
}