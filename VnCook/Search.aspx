﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="VnCook.Search" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <title>VnCook - Hướng dẫn nấu ăn</title>
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/Index.css">
    <link rel="icon" href="images/icon.png"> 
    <script src="javascript/index.js"></script>
    <script src="javascript/javascript.js"></script>
</head>
<body>
    <div class = "narbar">
        <button id = "menu" onclick="menuClick()">MENU</button>
        <img src="images/banner.png" alt="banner" onclick="location.href = 'Index.aspx'">
        <div class="grid">
            <div class= "search" id = "idSearch">
                <input type="text" placeholder="Tìm tên món" onkeypress="return runScript(event)" id="textSearch">
                <button></button>    
            </div>     
            <div class="get-user" id = "idGetUser">
                <input class = "button" type="button" value="Đăng ký/ Đăng Nhập" id="buttonSignIn" runat="server" onclick="location.href = 'SignIn.aspx'">
                <div class="main-user" id="mainUser" runat="server">
                    <p id="userName" runat="server">Phạm Tiến Đạt</p>
                    <img id="userAvatar" runat="server" src="images/user.png">
                </div>
            </div>
        </div>
    </div>
    <hr>

    <img src="images/banner22.png" alt="banner2" class="banner"> 

    <div class="mid">
        <div class = "mid-1">
            <h2 id ="textKetQua" runat="server"></h2>
            <br/>
        </div>

        <div class = "mid-2" runat ="server" id="dsMonAn">
            
        </div>
    </div>

    <footer>
        <p>Bản quyền của © VnCooK Inc. All Rights Reserved </p>
    </footer>
    
    </body>
</html>