﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="VnCook.TrangChu" EnableViewState="false" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <title>VnCook - Hướng dẫn nấu ăn</title>
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/Index.css">
    <link rel="icon" href="images/icon.png"> 
    <script src="javascript/index.js"></script>
    <script src="javascript/javascript.js"></script>
</head>
<body>
    <div class = "narbar">
        <button id = "menu" onclick="menuClick()">MENU</button>
        <img src="images/banner.png" alt="banner" onclick="location.href = 'Index.aspx'">
        <div class="grid">
            <div class= "search" id = "idSearch">
                <input type="text" placeholder="Tìm tên món" onkeypress="return runScript(event)" id="textSearch">
                <button></button>    
            </div>     
            <div class="get-user" id = "idGetUser">
                <input class = "button" type="button" value="Đăng ký/ Đăng Nhập" id="buttonSignIn" runat="server" onclick="location.href = 'SignIn.aspx'">
                <div class="main-user" id="mainUser" runat="server">
                    <p id="userName" runat="server">Phạm Tiến Đạt</p>
                    <img id="userAvatar" runat="server" src="images/user.png">
                </div>
            </div>
        </div>
    </div>
    <hr>

    <img src="images/banner22.png" alt="banner2" class="banner"> 

    <div class="mid">
        <form method="GET" runat="server" EnableViewState="false">
            <div class = "mid-1">
                <figure>
                    <label for= "buaAn">Bữa ăn</label>
                    <select id = "buaAn" runat ="server" EnableViewState="false">  
                        <option value="0">Tất cả</option>
                    </select>
                </figure>
                    
                <figure>
                    <label for="loaiMonAn">Phân Loại</label>
                    <select id = "loaiMonAn" runat="server" EnableViewState="false">
                        <option value="0">Tất cả</option>
                    </select>
                </figure> 
                    
                <figure>
                    <label for="thoiGianNau">Thời gian</label>
                    <select id = "thoiGianNau" runat="server" EnableViewState="false">
                        <option value="0">Tất cả</option>
                    </select>
                </figure>

                <input type="submit" class="button" value="Lọc" runat="server" onserverclick="btnLocClick" EnableViewState="false">
        </div>
        </form>

        <div class = "mid-2" runat ="server" id="dsMonAn">
            <div class = "mon-an">
                <div class="author">
                    <img src="images/user.png">
                    <div>
                        <b>Phạm Tiến Đạt</b>
                        <p>11/07/2020</p>
                    </div>
                </div>
                <hr>

                <h4><a href="#">Bơ hấp trứng cách thủy</a></h4>
                <img src="https://img-global.cpcdn.com/recipes/404d11c5f17d120d/640x640sq70/photo.jpg">
                <figure>
                    <img src="images/bua-an.png">
                    <figcaption>Bữa sáng</figcaption>
                </figure>
                <figure>
                    <img src="images/loai.png">
                    <figcaption>Món nấu</figcaption>
                </figure>
                <figure>
                    <img src="images/thoi-gian.png">
                    <figcaption>15 phút</figcaption>
                </figure>
                <figure>
                    <img src="images/view.png">
                    <figcaption>10 lượt xem</figcaption>
                </figure>
            </div>

        </div>
    </div>

    <footer>
        <p>Bản quyền của © VnCooK Inc. All Rights Reserved </p>
    </footer>
    
    </body>
</html>