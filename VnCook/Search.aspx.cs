﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VnCook
{
    public partial class Search : System.Web.UI.Page
    {
        Dictionary<string, string> mapComboBox = new Dictionary<string, string>();

        protected void Page_Load(object sender, EventArgs e)
        {

            xuLyNarBar();

            DataTable dt = new DataTable();
            //Lấy danh sách các bữa ăn
            dt = DataUtils.getAllDataFromTable("tbl_bua_an");
            foreach (DataRow row in dt.Rows)
            {
                mapComboBox.Add(row["bua_an_code"].ToString(), row["label_bua_an"].ToString());
            }

            //Lấy danh sách loại món ăn
            dt = DataUtils.getAllDataFromTable("tbl_loai_mon_an");
            foreach (DataRow row in dt.Rows)
            {
                mapComboBox.Add(row["loai_mon_an_code"].ToString(), row["label_loai_mon_an"].ToString());
            }

            //Lấy danh sách các bữa ăn
            dt = DataUtils.getAllDataFromTable("tbl_thoi_gian_nau");
            foreach (DataRow row in dt.Rows)
            {
                mapComboBox.Add(row["thoi_gian_nau_code"].ToString(), row["label_thoi_gian_nau"].ToString());
            }

            textKetQua.InnerText = "Kết quả tìm kiếm của từ khóa \"" + Request.QueryString.Get("tenMonAn") + "\"";
            loadSearch(Request.QueryString.Get("tenMonAn"));
        }


        protected void loadSearch(String textSearch)
        {
            StringBuilder html = new StringBuilder();
            StringBuilder sql = new StringBuilder("SELECT * FROM tbl_mon_an");
            sql.Append(" WHERE ten_mon_an LIKE " + DataUtils.getStringSqlVi("%"+textSearch+"%"));
            sql.Append(" AND status = " + DataUtils.getStringSql(Constants.STATUS_ACTIVE));
            sql.Append(" ORDER BY approved_date DESC");

            DataTable dt = DataProvider.execQuery(sql.ToString());

            foreach (DataRow row in dt.Rows)
            {
                html.Append("<div class = 'mon-an'>");
                html.Append("<div class = 'author'>");

                DataTable dtt = DataUtils.getUserInforFromUserCode(row["author"].ToString());

                String avatar = dtt.Rows[0]["avatar"].ToString();

                html.Append("<img src='" + avatar + "'>");
                html.Append("<div>");
                html.Append("<a href = 'Profile.aspx?userCode=" + dtt.Rows[0]["user_code"].ToString() + "'>");
                html.Append("<b>" + dtt.Rows[0]["ho_ten"].ToString() + "</b></a>");
                html.Append("<p>" + DataUtils.formatDate(row["approved_date"].ToString()) + "</p>");
                html.Append("</div></div><hr>");
                html.Append("<h4><a href = 'FoodInfo.aspx?foodCode=" + row["mon_an_code"].ToString() + "'>" + row["ten_mon_an"].ToString() + "</a></h4>");
                html.Append("<img src='" + row["image"].ToString() + "'>");

                html.Append("<div class='status'>");

                html.Append("<figure>");
                html.Append("<img src='images/bua-an.png'>");
                html.Append("<figcaption>" + mapComboBox[row["bua_an_code"].ToString()] + "</figcaption>");
                html.Append("</figure>");

                html.Append("<figure>");
                html.Append("<img src='images/loai.png'>");
                html.Append("<figcaption>" + mapComboBox[row["loai_mon_an_code"].ToString()] + "</figcaption>");
                html.Append("</figure>");

                html.Append("<figure>");
                html.Append("<img src='images/thoi-gian.png'>");
                html.Append("<figcaption>" + mapComboBox[row["thoi_gian_nau_code"].ToString()] + "</figcaption>");
                html.Append("</figure>");

                html.Append("<figure>");
                html.Append("<img src='images/view.png'>");
                html.Append("<figcaption>" + row["luot_xem"].ToString() + " lượt xem" + "</figcaption>");
                html.Append("</figure>");

                html.Append("</div></div>");
            }

            dsMonAn.InnerHtml = html.ToString();

        }

        protected void xuLyNarBar()
        {
            if (Session["userCode"] != null)
            {
                userName.InnerHtml = "<a href = 'Profile.aspx?userCode=" + Session["userCode"].ToString() + "'>" + Session["userName"].ToString() + "</a>";
                userAvatar.Src = Session["userAvatar"].ToString();
                mainUser.Style.Add("display", "flex");
                buttonSignIn.Style.Add("visibility", "hidden");
            }
        }
    }
}