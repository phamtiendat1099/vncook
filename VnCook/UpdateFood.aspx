﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateFood.aspx.cs" Inherits="VnCook.UpdateFood" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <title>VnCook - Hướng dẫn nấu ăn</title>
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/UpdateFood.css">
    <link rel="icon" href="images/icon.png"> 
    <script src="javascript/javascript.js"></script>
    <script src="javascript/UpdateFood.js"></script>
</head>
<body>
    <div class = "narbar">
        <button id = "menu" onclick="menuClick()">MENU</button>
        <img src="images/banner.png" alt="banner" onclick="location.href = 'Index.aspx'">
        <div class="grid">
            <div class= "search" id = "idSearch">
                <input type="text" placeholder="Tìm tên món" onkeypress="return runScript(event)" id="textSearch">
                <button></button>    
            </div>     
            <div class="get-user" id = "idGetUser">
                <input class = "button" type="button" value="Đăng ký/ Đăng Nhập" id="buttonSignIn" runat="server" onclick="location.href = 'SignIn.aspx'">
                <div class="main-user" id="mainUser" runat="server">
                    <p id="userName" runat="server">Phạm Tiến Đạt</p>
                    <img id="userAvatar" runat="server" src="images/user.png">
                </div>
            </div>
        </div>
    </div>
    <hr>


    <!--<img src="images/banner22.png" alt="banner2" class="banner">-->
    
    <form method="POST" runat="server">
        <fieldset id="fieldSet" runat="server">
            <legend id="legend" runat="server">Sửa, Thêm Mới</legend>
            <div class="mid">
                <div class="mid-left">
                    <div class="mon-an">
                        <label for="tenMonAn">Tên món ăn: </label>
                        <input type="text" id="tenMonAn" placeholder="Nhập tên món ăn ..." class="txt" runat="server">
                    </div>

                    <figure>
                        <label for= "buaAn">Bữa ăn</label>
                        <select id = "buaAn" runat ="server">  
                        </select>
                    </figure>
                    
                    <figure>
                        <label for="loaiMonAn">Phân Loại</label>
                        <select id = "loaiMonAn" runat="server">
                        </select>
                    </figure> 
                    
                    <figure>
                        <label for="thoiGianNau">Thời gian</label>
                        <select id = "thoiGianNau" runat="server">
                        </select>
                    </figure>
                    <br>
                    
                    <label for="img">Chọn Ảnh</label>
                    <input type="file" title="" id="img" name="img" accept="image/*" runat="server" onchange="preview_image(event)" >
                    <p id ="loiAnh" runat="server"></p>
                    <img id="imgPreview" runat="server">
                </div>
                <!-- END MID LEFT RIGHT -->

                <div class="mid-right">
                    <label for="txtNguyenLieu">Nguyên Liệu</label>
                    <textarea id="txtNguyenLieu" placeholder="Nhập nguyên liệu ..." runat="server"></textarea>
                    <label for="txtCachLam">Cách làm</label>
                    <textarea id="txtCachLam" placeholder="Nhập cách làm ..." runat="server"></textarea>

                </div>
                <!-- END MID RIGHT CLASS -->
            </div>
            <!-- END MID CLASS -->
            
            <hr>
            <div class="list-button">
                <input type="submit" id="btnSubmit" runat="server" class="button button-edit" value="Thêm" onserverclick="btnSubmitClick">    
                <input type="reset" class="button button-delete" value="Reset">    
            </div>
            
        </fieldset>
        <!-- END FIELD SET -->
    </form>
    <!-- END FORM -->

    <footer>
        <p>Bản quyền của © VnCooK Inc. All Rights Reserved </p>
    </footer>
    
</body>
</html>
