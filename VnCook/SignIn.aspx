﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignIn.aspx.cs" Inherits="VnCook.SignIn" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <title>VnCook - Hướng dẫn nấu ăn</title>
    <link rel="stylesheet" href="styles/SignIn.css">
    <link rel="stylesheet" href="styles/Style.css">
    <link rel="icon" href="images/icon.png"> 
    <script src="javascript/javascript.js"></script>
</head>
<body>
    <div class = "narbar">
        <button id = "menu" onclick="menuClick()">MENU</button>
        <img src="images/banner.png" alt="banner" onclick="location.href = 'Index.aspx'">
        <div class="grid">
            <div class= "search" id = "idSearch">
                <input type="text" placeholder="Tìm tên món" onkeypress="return runScript(event)" id="textSearch">
                <button></button>    
            </div>     
            <div class="get-user" id = "idGetUser">
                <input class = "button" type="button" value="Đăng ký/ Đăng Nhập" id="buttonSignIn" runat="server" onclick="location.href = 'SignIn.aspx'">
                <div class="main-user" id="mainUser" runat="server">
                    <p id="userName" runat="server">Phạm Tiến Đạt</p>
                    <img id="userAvatar" runat="server" src="images/user.png">
                </div>
            </div>
        </div>
    </div>
    <hr>


    <div id="notification" runat="server">
    </div>

    <img src="images/banner22.png" alt="banner2" class="banner">

    <form method="POST" runat ="server">
        <div class="midd">
            <div class="dang-nhap">
                <h2>Đăng Nhập</h2>
                <br>
                <p id = "loiDangNhap" runat="server"></p>
            
                <input type="text" class="textbox" placeholder="Email" name = "dang-nhap-email">
                <input type="password" class="textbox" placeholder="Mật khẩu" name ="dang-nhap-password">
                <br>
                <input type="checkbox" class="checkbox" id = "nho-mat-khau">
                <label for = "nho-mat-khau">Ghi Nhớ Mật khẩu ?</label>
                <br><br>
                <input type="submit" runat="server" value = "Đăng Nhập" class="button" onserverclick="signInClick">       
            </div>

            <div class="dang-ky">
                <h2>Đăng ký</h2>
                <br>
                <p id = "loiDangKy" runat="server"></p>

                <input type="text" class="textbox" placeholder="Email" name="dang-ky-email">
                <input type="password" class="textbox" placeholder="Mật khẩu" name ="dang-ky-password">
                <input type="password" class="textbox" placeholder="Nhập lại khẩu" name = "re-dang-ky-password">
                <br>
                <input type="submit" value = "Đăng Ký" class="button" runat="server" onserverclick="signUpClick">
       
            </div>
        </div>
    </form>

    <footer>
        <p>Bản quyền của © VnCooK Inc. All Rights Reserved </p>
    </footer>
    
</body>
</html>