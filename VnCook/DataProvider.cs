﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VnCook
{
    public static class DataProvider
    {
        private static string connectString = @"Data Source=13.229.56.95;Initial Catalog=db_nau_an;User ID=btl_ltw;Password=123456";
        //private static string connectString = @"Data Source=localhost;Initial Catalog=db_nau_an;User ID=sa;Password=123456";

        public static DataTable execQuery(string query)
        {
            DataTable data = new DataTable();
            using (SqlConnection con = new SqlConnection(connectString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand(query, con);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                adapter.Fill(data);

                con.Close();
            }
            return data;
        }

        public static int execNonQuery(string query)
        {
            int data = 0;
            using (SqlConnection con = new SqlConnection(connectString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand(query, con);

                data = cmd.ExecuteNonQuery();

                con.Close();

            }
            return data;
        }
      
        public static object execScaler(string query)
        {
            object data = 0;
            using (SqlConnection con = new SqlConnection(connectString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand(query, con);

                data = cmd.ExecuteScalar();

                con.Close();

            }
            return data;
        }

    }
}
